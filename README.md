# Projet NoSQL

* Visualisation de la base de données UniProtKB
* Propagation de labels dans un graphe

## Back-End

* Pré-traitements à base de petits programmes en C
* Neo4j et scripts Cypher pour la BDD

## Front-End

* Python
* Flask
* d3.js
* Bootstrap

## Membres

* Syméon CARLE
* Pierre DENAUW

