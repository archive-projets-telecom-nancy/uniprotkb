import argparse
import gzip
import csv

parser = argparse.ArgumentParser()

parser.add_argument("input_file")
parser.add_argument("output_CSV")

args = parser.parse_args()

if not args.input_file.endswith(".gz"):
    input_file_open_func = open
else:
    input_file_open_func = gzip.open


with input_file_open_func(args.input_file, "rt") as entrée, open(
    args.output_CSV, "w"
) as sortie:

    protwriter = csv.writer(sortie, delimiter=",", quotechar='"')

    première_ligne = next(entrée).split("\t")
    prot = première_ligne[0]
    domaines = set([première_ligne[1]])

    for ligne_raw in entrée:
        ligne = ligne_raw.split("\t")
        if ligne[0] == prot:
            domaines.add(ligne[1])
        else:
            print(prot, end="\r")
            protwriter.writerow([prot, ";".join(domaines)])
            prot = ligne[0]
            domaines = set([ligne[1]])

    protwriter.writerow([prot, ";".join(domaines)])
    print("")
