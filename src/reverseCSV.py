import csv
import bisect
import sys

prot_reader = csv.reader(sys.stdin, delimiter=",", quotechar='"')
m = max((ligne[0] for ligne in prot_reader), key=len)
print(f"Longest protein name : {m}")
print(f"Length : {len(m)}")
