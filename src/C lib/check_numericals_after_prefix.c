#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{

    char *line = NULL;
    size_t size;
    int read;

    while ((read = getline(&line, &size, stdin)) != -1)
    {
        char *prot;
        char *dom;
        // premier token = protéine
        prot = strtok(line, "\t");
        // deuxième token = domaine
        if ((dom = strtok(NULL, "\t")) != NULL)
        {
            char *chiffres = &dom[3];
            for (int i = 0; chiffres[i] != '\0'; i++)
            {

                if ('0' > chiffres[i] || '9' < chiffres[i])
                {
                    printf("Non-numerical after prefix in protein %s : %s\n", prot, dom);
                    break;
                }
            }
        }
    }

    return 0;
}
