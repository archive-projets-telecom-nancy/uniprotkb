#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "vecteur.h"

int main(int argc, char const *argv[])
{

    bool premiere_prot = true;
    bool premier_go = true;
    char prot[50], go_term[50];
    char *line = NULL;
    size_t size;
    int read;
    FILE *stdout2 = fopen("/dev/tty", "w");

    while ((read = getline(&line, &size, stdin)) != -1)
    {
        if (line[0] == '!')
        {
            continue;
        }
        else
        {
            vecteur *tokens = new_vecteur(sizeof(char *));
            vect_append(tokens, strtok(line, "\t"));
            char *token;
            while ((token = strtok(NULL, "\t")) != NULL)
            {
                vect_append(tokens, token);
            }

            if (tokens->taille < 2)
            {
                fprintf(stdout2, "!! : %s", line);
                continue;
            }
            else
            {
                if (premiere_prot)
                {
                    premiere_prot = false;
                    strncpy(prot, (char *)tokens->contenu[1], 50);
                    printf("%s,\"", prot);
                }
                else
                {
                    if (strncmp(prot, (char *)tokens->contenu[1], 50) != 0)
                    {
                        premier_go = true;
                        printf("\"\n");
                        strncpy(prot, (char *)tokens->contenu[1], 50);
                        printf("%s,\"", prot);
                    }
                }
                if (premier_go)
                {
                    premier_go = false;
                    for (size_t i = 0; i < tokens->taille; i++)
                    {
                        if (strncmp((char *)tokens->contenu[i], "GO:", 3) == 0)
                        {
                            printf("%s", (char *)tokens->contenu[i]);
                            strncpy(go_term, (char *)tokens->contenu[i], 50);
                            break;
                        }
                    }
                }
                else
                {
                    for (size_t i = 0; i < tokens->taille; i++)
                    {
                        if (strncmp((char *)tokens->contenu[i], "GO:", 3) == 0)
                        {
                            if (strncmp(go_term, (char *)tokens->contenu[i], 50) != 0)
                            {
                                printf(";%s", (char *)tokens->contenu[i]);
                                strncpy(go_term, (char *)tokens->contenu[i], 50);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    printf("\"\n");
}