#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[]) {

	char prot[50], domain[50];
	char* line = NULL;
	size_t size;
	int read;
	FILE* stdout2 = fopen("/dev/tty", "w+");

	if ((read=getline(&line, &size, stdin)) != -1) {
		char* token;
		token = strtok(line, "\t");
		strcpy(prot, token);
		fprintf(stdout2,"\r%s",prot);
		printf("%s,\"",prot);
		if ((token = strtok(NULL, "\t")) != NULL) {
			strcpy(domain, token);
			printf("%s",domain);
		}
	}

	while ((read=getline(&line, &size, stdin)) != -1) {
		char* token;
		token = strtok(line, "\t");
		if (strcmp(token, prot) == 0) {
			if ((token = strtok(NULL, "\t")) != NULL) {
				if (strcmp(token, domain)) {
					printf(";%s",token);
					strcpy(domain, token);
				}
			}
		} else {
			printf("\"\n");
			strcpy(prot, token);
			fprintf(stdout2,"%s\r",prot);
			printf("%s,\"",prot);
			if ((token = strtok(NULL, "\t")) != NULL) {
				strcpy(domain, token);
				printf("%s",domain);
			}
		}	
	}
	printf("\"\n");
}