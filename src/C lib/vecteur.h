#ifndef VECTEUR_H
#define VECTEUR_H

typedef struct vecteur vecteur;

struct vecteur {
	void** contenu;
	int taille;
	int taille_reelle;
	int taille_realloc;
	int taille_element;
};

vecteur* new_vecteur(int taille_element);
void vect_append(vecteur* v, void* element);
void vect_copy_and_append(vecteur* v, void* element);
void* vect_pop(vecteur* v);
void** vect_to_NULL_terminated_array(vecteur* v);
void vect_print(vecteur* v,void (*element_printer)(void*));
int vect_index(vecteur* v, void* element, int (*comp_function)(void*,void*));
int vect_first_match(vecteur* v, int (*match_function)(void*));
void vect_merge_unique(vecteur* cible, vecteur* source, int (*comp_function)(void*,void*));
void vect_free(vecteur* v, void (*free_element)(void*));

#endif