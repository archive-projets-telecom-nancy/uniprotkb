#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "reverseCSV.h"

/*
*  Se base sur la structure du csv sorti par protein2ipr2csv
*/
int main(int argc, char const *argv[])
{
    char *line = NULL;
    size_t size;
    int read;
    linked_dom *domaines = new_linked_dom();

    FILE *stdout2 = fopen("/dev/tty", "w");

    FILE *fichier;
    if (argc > 1)
    {
        fichier = fopen(argv[1], "r");
    }
    else
    {
        fichier = stdin;
    }

    int prot_count = 0;
    while ((read = getline(&line, &size, fichier)) != -1)
    {
        prot *new_prot = (prot *)calloc(1, sizeof(prot));
        new_prot->nom = (char *)calloc(11, sizeof(char));
        new_prot->domaines = new_vecteur(sizeof(int));
        int i = 0;

        while (line[i] != ',' && line[i] != '\0')
        {
            i++;
        }

        if (line[i] != ',')
        {
            fprintf(stderr, "Error while scanning line : %s", line);
            fflush(stdout);
            continue;
        }

        strncpy(new_prot->nom, line, i);

        while (line[i] != 'I' && line[i] != '\0')
        {
            i++;
        }

        if (line[i] == '\0')
        {
            fprintf(stderr, "Couldn't find first domain after protein name : %s", line);
            fflush(stdout);
            continue;
        }

        // i doit à présent être au début du premier domaine

        do
        {
            int debut_domaine = i;
            while (line[i] != ';' && line[i] != '\"' && line[i] != '\0')
            {
                i++;
            }
            char nom_domaine[20];
            memset((void *)nom_domaine, 0, 20);
            strncpy(nom_domaine, &line[debut_domaine], i - debut_domaine);
            int domaine = atoi(&nom_domaine[3]);
            vect_copy_and_append(new_prot->domaines, (void *)&domaine);
            i++; // on passe au dessus du ';' (ou '\"')
        } while (line[i] != '\"' && line[i] != '\0' && line[i] != '\n');

        domaines = ajouter_prots(domaines, new_prot);
        prot_count++;
        fprintf(stdout2, "\r%s\t%d", new_prot->nom, prot_count);
    }
    fprintf(stdout2, "\n");

    linked_dom *i_dom = domaines;

    while (1)
    {
        printf("IPR%06d", i_dom->number);
        linked_prot *i_prot = i_dom->proteins;

        while (1)
        {
            printf(",%s", i_prot->name);
            if (i_prot->next == NULL)
            {
                break;
            }
            i_prot = i_prot->next;
        }

        printf("\n");
        if (i_dom->next == NULL)
        {
            break;
        }
        i_dom = i_dom->next;
    }

    return 0;
}

void int_printer(void *int_ptr)
{
    printf("%d", *(int *)int_ptr);
    fflush(stdout);
}

linked_dom *ajouter_prots(linked_dom *domaines, prot *new_prot)
{
    linked_dom *first_domain = domaines;
    for (size_t i = 0; i < new_prot->domaines->taille; i++)
    {
        int number = *(int *)new_prot->domaines->contenu[i];
        linked_dom *domaine = find_or_insert_dom(domaines, number);

        if (domaine->previous == NULL)
        {
            first_domain = domaine;
        }
        if (domaine->proteins == NULL)
        {
            domaine->proteins = new_linked_prot_(NULL, new_prot->nom, NULL);
            continue;
        }
        else
        {
            append_prot(domaine->proteins, new_prot->nom);
        }
    }
    return first_domain;
}

linked_dom *new_linked_dom()
{
    linked_dom *new_dom = (linked_dom *)calloc(1, sizeof(linked_dom));
    new_dom->previous = NULL;
    new_dom->number = -1;
    new_dom->proteins = NULL;
    new_dom->next = NULL;
    return new_dom;
}

linked_dom *new_linked_dom_(linked_dom *previous, int number, linked_prot *proteins, linked_dom *next)
{
    linked_dom *new_dom = new_linked_dom();
    new_dom->previous = previous;
    new_dom->number = number;
    new_dom->proteins = proteins;
    new_dom->next = next;
    return new_dom;
}

linked_prot *new_linked_prot()
{
    linked_prot *new_prot = (linked_prot *)calloc(1, sizeof(linked_prot));
    new_prot->previous = NULL;
    new_prot->next = NULL;
    return new_prot;
}

linked_prot *new_linked_prot_(linked_prot *previous, char *name, linked_prot *next)
{
    linked_prot *new_prot = new_linked_prot();
    new_prot->previous = previous;
    strncpy(new_prot->name, name, 10);
    new_prot->name[10] = '\0';
    new_prot->next = next;
    return new_prot;
}

linked_dom *find_or_insert_dom(linked_dom *domaines, int number)
{
    if (domaines->number == -1)
    {
        domaines->number = number;
        return domaines;
    }
    else
    {

        linked_dom *i_dom = domaines;
        while (i_dom->number < number && i_dom->next != NULL)
        {
            i_dom = i_dom->next;
        }

        if (i_dom->number == number)
        {
            return i_dom;
        }
        else if (i_dom->number < number)
        {
            linked_dom *new_dom = new_linked_dom_(i_dom, number, NULL, i_dom->next);
            i_dom->next = new_dom;
            return new_dom;
        }
        else if (i_dom->number > number)
        {
            linked_dom *new_dom = new_linked_dom_(i_dom->previous, number, NULL, i_dom);
            if (i_dom->previous != NULL)
            {
                i_dom->previous->next = new_dom;
                i_dom->previous = new_dom;
            }
            return new_dom;
        }
        else
        {
            fprintf(stderr, "WTF ?");
            exit(-1);
        }
    }
}

void append_prot(linked_prot *proteins, char *name)
{
    linked_prot *i_prot = proteins;
    while (i_prot->next != NULL)
    {
        i_prot = i_prot->next;
    }
    i_prot->next = new_linked_prot_(i_prot, name, NULL);
}
