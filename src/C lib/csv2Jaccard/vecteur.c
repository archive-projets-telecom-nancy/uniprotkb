#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vecteur.h"

#define TAILLE_DEPART 4
#define TAILLE_REALLOC 4

vecteur *new_vecteur(int taille_element)
{

	vecteur *v;
	vecteur *tmp = (vecteur *)calloc(1, sizeof(vecteur));
	if (tmp == NULL)
	{
		printf("Erreur lors de l'allocation d'un vecteur");
		free(tmp);
		exit(-1);
	}
	else
	{
		v = tmp;
	}

	void **chaine_depart;
	void **tmp_ = (void **)calloc(TAILLE_DEPART, sizeof(void *));
	if (tmp_ == NULL)
	{
		printf("Erreur lors de l'allocation du tableau de contenu d'un vecteur\n");
		free(tmp_);
		exit(-1);
	}
	else
	{
		chaine_depart = tmp_;
	}

	v->contenu = chaine_depart;

	v->taille = 0;
	v->taille_reelle = TAILLE_DEPART;
	v->taille_realloc = TAILLE_REALLOC;
	v->taille_element = taille_element;

	return v;
}

void vect_append(vecteur *v, void *element)
{

	// Si on doit réallouer
	if (v->taille == v->taille_reelle)
	{

		v->taille_reelle += v->taille_realloc;
		v->contenu = realloc(v->contenu, v->taille_reelle * sizeof(void *));

		if (v->contenu == NULL)
		{
			printf("Erreur lors de la réallocation du tableau de contenu d'un vecteur\n");
			free(v->contenu);
			exit(-1);
		}
	}

	v->contenu[v->taille] = element;
	v->taille += 1;
}

void vect_copy_and_append(vecteur *v, void *element)
{

	// Si on doit réallouer
	if (v->taille == v->taille_reelle)
	{

		v->taille_reelle += v->taille_realloc;
		v->contenu = realloc(v->contenu, v->taille_reelle * sizeof(void *));

		if (v->contenu == NULL)
		{
			printf("Erreur lors de la réallocation du tableau de contenu d'un vecteur\n");
			free(v->contenu);
			exit(-1);
		}
	}

	void *copie;
	void *tmp = (void *)malloc(v->taille_element);
	if (tmp != NULL)
	{
		copie = tmp;
	}
	else
	{
		printf("Erreur lors de l'allocation d'un nouvel element de vecteur");
		free(tmp);
		exit(-1);
	}

	v->contenu[v->taille] = memcpy(copie, element, v->taille_element);

	v->taille += 1;
}

void *vect_pop(vecteur *v)
{

	if (v->taille < 0)
	{
		return NULL;
	}
	else
	{
		void *copie;
		void *tmp = (void *)malloc(v->taille_element);
		if (tmp != NULL)
		{
			copie = tmp;
		}
		else
		{
			printf("Erreur lors de l'allocation d'un element sortant du vecteur");
			free(tmp);
			exit(-1);
		}
		copie = memcpy(copie, v->contenu[v->taille - 1], v->taille_element);
		free(v->contenu[v->taille - 1]);
		v->taille -= 1;
		return copie;
	}
}

void **vect_to_NULL_terminated_array(vecteur *v)
{

	void **res;
	void **tmp = (void **)malloc(sizeof(void *) * (v->taille + 1));
	if (tmp != NULL)
	{
		res = tmp;
	}
	else
	{
		printf("Erreur lors de l'allocation du void** dans vect_to_NULL_terminated_array()");
		free(tmp);
		exit(-1);
	}

	int i;
	for (i = 0; i < v->taille; i++)
	{
		res[i] = v->contenu[i];
	}
	res[v->taille] = NULL;
	return res;
}

void vect_print(vecteur *v, void (*element_printer)(void *))
{
	printf("[");
	for (int i = 0; i < v->taille; ++i)
	{
		if (i != 0)
		{
			printf(",");
		}
		element_printer(v->contenu[i]);
	}
	printf("]\n");
}

int vect_index(vecteur *v, void *element, int (*comp_function)(void *, void *))
{
	for (int i = 0; i < v->taille; i++)
	{
		if (comp_function(element, v->contenu[i]))
		{
			return i;
		}
	}
	return -1;
}

int vect_first_match(vecteur *v, int (*match_function)(void *))
{
	for (int i = 0; i < v->taille; i++)
	{
		if (match_function(v->contenu[i]))
		{
			return i;
		}
	}
	return -1;
}

void vect_merge_unique(vecteur *cible, vecteur *source, int (*comp_function)(void *, void *))
{
	for (int i = 0; i < source->taille; i++)
	{
		if (vect_index(cible, source->contenu[i], comp_function) == -1)
		{
			vect_copy_and_append(cible, source->contenu[i]);
		}
	}
}

void vect_free(vecteur *v, void (*free_element)(void *))
{

	if (free_element != NULL)
	{
		for (int i = 0; i < v->taille; i++)
		{
			free_element(v->contenu[i]);
		}
	}
	free(v->contenu);
	free(v);
}