#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "csv2jaccard.h"

/*
*  Se base sur la structure du csv sorti par protein2ipr2csv
*/
int main(int argc, char const *argv[])
{
    char *line = NULL;
    size_t size;
    int read;
    vecteur *proteins = new_vecteur(sizeof(prot));

    while ((read = getline(&line, &size, stdin)) != -1)
    {
        prot *new_prot = (prot *)calloc(1, sizeof(prot));
        new_prot->nom = (char *)calloc(11, sizeof(char));
        new_prot->domaines = new_vecteur(sizeof(int));
        int i = 0;

        while (line[i] != ',' && line[i] != '\0')
        {
            i++;
        }

        if (line[i] != ',')
        {
            fprintf(stderr, "Error while scanning line : %s", line);
            continue;
        }

        strncpy(new_prot->nom, line, i);

        while (line[i] != 'I' && line[i] != '\0')
        {
            i++;
        }

        if (line[i] == '\0')
        {
            fprintf(stderr, "Couldn't find first domain after protein name : %s", line);
            continue;
        }

        // i doit à présent être au début du premier domaine

        do
        {
            int debut_domaine = i;
            while (line[i] != ';' && line[i] != '\"' && line[i] != '\0')
            {
                i++;
            }
            char nom_domaine[10];
            memset((void *)nom_domaine, 0, 10);
            strncpy(nom_domaine, &line[debut_domaine], i - debut_domaine);
            int domaine = atoi(&nom_domaine[3]);
            vect_copy_and_append(new_prot->domaines, (void *)&domaine);
            i++; // on passe au dessus du ';' (ou '\"')
        } while (line[i] != '\"' && line[i] != '\0' && line[i] != '\n');

        vect_append(proteins, new_prot);
    }

    for (size_t i = 0; i < proteins->taille; i++)
    {
        for (size_t j = i + 1; j < proteins->taille; j++)
        {
            prot *prot_a = (prot *)proteins->contenu[i];
            prot *prot_b = (prot *)proteins->contenu[j];
            int a = 0;
            int b = 0;
            int taille_intersection = 0;
            while (a < prot_a->domaines->taille && b < prot_b->domaines->taille)
            {
                int domaine_a = *(int *)prot_a->domaines->contenu[a];
                int domaine_b = *(int *)prot_b->domaines->contenu[b];
                if (domaine_a == domaine_b)
                {
                    taille_intersection++;
                    a++;
                    b++;
                }
                else if (domaine_a > domaine_b)
                {
                    b++;
                }
                else if (domaine_a < domaine_b)
                {
                    a++;
                }
                else
                {
                    fprintf(stderr, "WTF ?");
                    exit(-1);
                }
            }
            if (taille_intersection > 0)
            {
                int taille_union = prot_a->domaines->taille + prot_b->domaines->taille - taille_intersection;
                double jaccard_index = ((double)taille_intersection) / ((double)taille_union);
                printf("%s,%s,%f,%d/%d\n", prot_a->nom, prot_b->nom, jaccard_index, taille_intersection, taille_union);
            }
        }
    }

    return 0;
}

void int_printer(void *int_ptr)
{
    printf("%d", *(int *)int_ptr);
}
