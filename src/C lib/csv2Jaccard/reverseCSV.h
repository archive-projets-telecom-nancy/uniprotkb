#pragma once

#include "vecteur.h"

typedef struct linked_dom linked_dom;
typedef struct linked_prot linked_prot;

struct linked_dom
{
    linked_dom *previous;
    int number;
    linked_prot *proteins;
    linked_dom *next;
};

struct linked_prot
{
    linked_prot *previous;
    char name[11];
    linked_prot *next;
};

typedef struct prot prot;

struct prot
{
    char *nom;
    vecteur *domaines;
};

linked_dom *new_linked_dom();
linked_dom *new_linked_dom_(linked_dom *previous, int number, linked_prot *proteins, linked_dom *next);
linked_prot *new_linked_prot();
linked_prot *new_linked_prot_(linked_prot *previous, char *name, linked_prot *next);
linked_dom *ajouter_prots(linked_dom *domaines, prot *new_prot);
linked_dom *find_or_insert_dom(linked_dom *domaines, int number);
void append_prot(linked_prot *proteins, char *name);