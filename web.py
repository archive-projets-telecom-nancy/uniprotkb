#! /usr/bin/python
# -*- coding:utf-8 -*-

from flask import *
from flask_bootstrap import Bootstrap
from py2neo import Graph

graph = Graph(password = "1234")

def create_app():
  app = Flask(__name__)
  Bootstrap(app)

  return app
app = create_app()

@app.route('/', methods=['GET', 'POST'])
def home():
	if request.method == "POST":
		return redirect(url_for('prot',name_prot=request.form['search']))
	else:
		return render_template('accueil.html')

@app.route('/proteines')
def index_prot():
	results = graph.run("MATCH (n:Protein) RETURN n.name as name LIMIT 25")
	return render_template('proteines/index.html', results = results)


@app.route('/proteines/<name_prot>')
def prot(name_prot):
	prot = graph.run(f"MATCH (n:Protein) WHERE n.name=\'{name_prot}\' RETURN n.name as name LIMIT 25")
	neighbours = graph.run(f"match (n:Protein)-[:NEIGHBOURS]-(m:Protein) WHERE n.name=\'{name_prot}\' RETURN m.name as name")
	neighbours_neighbours = graph.run(f"match (n:Protein)-[:NEIGHBOURS*2]-(m:Protein) WHERE n.name=\'{name_prot}\' RETURN m.name as name")
	query = graph.run(f"match (n:Protein)-[r:NEIGHBOURS]-() WHERE n.name=\'{name_prot}\' RETURN COUNT(r) as count")
	query.forward()
	neighbours_count = query.current['count']
	query2 = graph.run(f"match (n:Protein)-[r:NEIGHBOURS*2]-() WHERE n.name=\'{name_prot}\' RETURN COUNT(r) as count")
	query2.forward()
	neighbours_neighbours_count = query2.current['count']
	return render_template('proteines/show.html', name_prot = name_prot, neighbours = neighbours, neighbours_neighbours = neighbours_neighbours, graph = prot_graph(name_prot).json, neighbours_count = neighbours_count, neighbours_neighbours_count = neighbours_neighbours_count)

@app.route('/proteines/<name_prot>/graph.json')
def prot_graph(name_prot):
	results = graph.run(f"match (n:Protein)-[r:NEIGHBOURS]-(m:Protein) WHERE n.name=\'{name_prot}\' RETURN m.name as name, r.weight as weight LIMIT 20")
	nodes = []
	relationship = []
	i = 0
	nodes.append({"caption": name_prot, "role":"root", "id":i})
	for neighbours in results:
		i += 1
		nodes.append(({"caption": neighbours["name"], "role":"neighbours", "id":i}))
		relationship.append({"source": 0, "target": i, "role":"neighbours", "caption":neighbours['weight']})
		name = neighbours["name"]
		neighbours_neighbours = graph.run(f"match (n:Protein)-[r:NEIGHBOURS]-(m:Protein) WHERE n.name=\'{name}\' RETURN m.name as name, r.weight as weight LIMIT 20")
		j=i
		for neighbours2 in neighbours_neighbours:
			i+=1
			nodes.append(({"caption": neighbours2["name"], "role":"neighbours of neighbours", "id":i}))
			relationship.append({"source": i, "target": j, "caption":neighbours_neighbours['weight'], "role":"neighbours_neighbours"})
	return jsonify({"nodes": nodes, "edges": relationship})


@app.route('/domaines')
def index_dom():
	results = graph.run("MATCH (n:Label) RETURN n.name as name LIMIT 25")
	return render_template('domaines/index.html', results = results)

if __name__ == '__main__':
    app.run(debug=True)
